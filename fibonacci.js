
module.exports = class Fibonacci {
    calculateFibonacci(n) {
        if (n === 1) {
            return 0;
        } else if (n === 2) {
            return 1;
        } else {
            return this.calculateFibonacci(n - 1) + this.calculateFibonacci(n - 2);
        }
    }
}
