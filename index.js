const yargs = require('yargs');
const Fibonacci = require('./fibonacci.js');

module.exports = () => {
  const options = yargs
   .usage("Usage: -n <nº-number>")
   .option("n", { alias: "fibonacci", describe: "Fibonacci sequence to find", type: "string", demandOption: true })
   .argv;

  const fb = new Fibonacci();
    
  console.log(fb.calculateFibonacci(options.n));
}