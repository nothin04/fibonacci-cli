import Fibonacci from "./fibonacci.js"

describe('Fibonacci N Finder', () => {
    describe('Units', () => {
      test('the 5º number should return 3', () => {
        const fib = new Fibonacci();
        expect(fib.calculateFibonacci(5)).toEqual(3);
      });
      
      test('the 10º number should return 34', () => {
        const fib = new Fibonacci();
        expect(fib.calculateFibonacci(10)).toEqual(34);
      });

      test('the 1º number should return 0', () => {
        const fib = new Fibonacci();
        expect(fib.calculateFibonacci(1)).toEqual(0);
      });
    });
});
